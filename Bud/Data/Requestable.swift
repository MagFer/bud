//
//  Requestable.swift
//  Bud
//
//  Created by Ian Andoni Magarzo Fernández on 3/10/18.
//  Copyright © 2018 Ian Magarzo Fernández. All rights reserved.
//

import Foundation
import UIKit

typealias RequestableClient = Client & Requestable

protocol Client: class {
    static var shared: RequestableClient! { get }
}

protocol Requestable {
    func bankImage(for stringURL: String, completion: @escaping (UIImage?)->())
    func getTransctions(withCompletion completion: @escaping ([Transation]?)->())
}
