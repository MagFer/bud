//
//  DataProvider.swift
//  Bud
//
//  Created by Ian Andoni Magarzo Fernández on 3/10/18.
//  Copyright © 2018 Ian Magarzo Fernández. All rights reserved.
//

import Foundation

enum DataProviderType {
    case NetworkMock
    case LocalMock
}

class DataProvider {
    
    static var shared: RequestableClient!

    init(dataChannelType: DataProviderType) {
        switch dataChannelType {
        case .LocalMock:
            DataProvider.shared = MockClient.shared
        case .NetworkMock:
            DataProvider.shared = NetworkMockClient.shared
        }
    }
}
