//
//  MockClient.swift
//  Bud
//
//  Created by Ian Andoni Magarzo Fernández on 3/10/18.
//  Copyright © 2018 Ian Magarzo Fernández. All rights reserved.
//

import Foundation
import UIKit

class MockClient: RequestableClient {
    
    static var shared: RequestableClient! = MockClient()
    
}

extension MockClient {
    
    func getTransctions(withCompletion completion: @escaping ([Transation]?) -> ()) {
        do {
            let jsonData = transactionsJSONString.data(using: .utf8)
            let resultDic = try! JSONSerialization.jsonObject(with: jsonData!) as! [String: Any]
            let transactionsDic = resultDic["data"]
            let transactionsData = try! JSONSerialization.data(withJSONObject: transactionsDic!, options: .prettyPrinted)
            let transactions = try JSONDecoder().decode([Transation].self, from: transactionsData)
            DispatchQueue.global().asyncAfter(deadline: .now() + 1.0) {
                completion(transactions)
            }
        } catch {
            print("Error: \(error)")
            completion(nil)
        }
    }
    
    func bankImage(for stringURL: String, completion: @escaping (UIImage?) -> ()) {
        completion(#imageLiteral(resourceName: "Bank Example Image"))
    }
}

