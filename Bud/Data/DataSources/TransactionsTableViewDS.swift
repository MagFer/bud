//
//  DataSource.swift
//  Bud
//
//  Created by Ian Andoni Magarzo Fernández on 5/10/18.
//  Copyright © 2018 Ian Magarzo Fernández. All rights reserved.
//

import Foundation
import UIKit

class TransactionsTableViewDataSource: NSObject, UITableViewDataSource {
    
    weak var transactionsWorker: TransactionsWorker!
    
    required init(worker: TransactionsWorker) {
        super.init()
        transactionsWorker = worker
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return transactionsWorker.transactions?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TransactionCellId", for: indexPath) as! TransactionTableViewCell
        if let transaction = transactionsWorker.transactions?[indexPath.row] {
            cell.descriptionLabel.text = transaction.description
            if let currencyValue = transaction.currency?.value {
                let currencyString = NumberFormatter.BudNumberFormatter().string(from: NSNumber(value: currencyValue))
                let currencyIsoString: String = CurrencyType.symbol(forCurrencyISO: CurrencyType.GBP)
                cell.costLabel?.text = (currencyString ?? "-") + " " + currencyIsoString
                
            }
            if let iconURLString = transaction.product?.icon {
                transactionsWorker.bankImage(for: iconURLString, completion: { (bankImage) in
                    if let bankImg = bankImage {
                        cell.bankImage.image = bankImg
                    }
                })
            }
        }
        return cell
    }
    
}
