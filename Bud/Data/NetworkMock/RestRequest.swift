//
//  RestRequest.swift
//  Bud
//
//  Created by Ian Andoni Magarzo Fernández on 5/10/18.
//  Copyright © 2018 Ian Magarzo Fernández. All rights reserved.
//

import Foundation

enum RequestError: Error {
    case invalidURLFormating
    case responseError
    case invalidClient
    case invalidJSONCasting
}

class HTTPRequest {
    var params: [String: String]?
    let endPoint: String?
    let baseURL: String
    
    init(baseURL:String, endpoint: String?, params: [String: String]?) {
        self.baseURL = baseURL
        self.endPoint = endpoint
        self.params = params
    }
    
    public func sendRequest(completion: @escaping (Response) -> ()) throws {
        var urlComponents = URLComponents(string: baseURL + (endPoint ?? ""))
        urlComponents?.queryItems = params?.compactMap{ URLQueryItem(name: $0.key, value: $0.value) }
        guard let urlComp = urlComponents,
            let url = urlComp.url else {
                throw RequestError.invalidURLFormating
        }
        let urlRequest = URLRequest(url: url,
                                    cachePolicy: .returnCacheDataElseLoad,
                                    timeoutInterval: 5)
        
        guard let connectionClient = NetworkMockClient.shared as? Connectable else {
            completion(Response(data: nil, urlResponse: nil, error: RequestError.invalidURLFormating))
            return
        }
        
        connectionClient.perform(request: urlRequest) { (response) in
            completion(response)
        }
    }
    
}
