//
//  Requests.swift
//  Bud
//
//  Created by Ian Andoni Magarzo Fernández on 5/10/18.
//  Copyright © 2018 Ian Magarzo Fernández. All rights reserved.
//

import Foundation
import UIKit

extension NetworkMockClient {
    
    func getTransctions(withCompletion completion: @escaping ([Transation]?) -> ()) {
        do {
            try HTTPRequest(baseURL: kBaseURLBud,
                        endpoint: "5b33bdb43200008f0ad1e256",
                        params: nil).sendRequest { (response) in
                            print(response)
                            guard let data = response.data else {
                                completion(nil)
                                return
                            }
                            do {
                                let resultDic = try JSONSerialization.jsonObject(with: data) as! [String: Any]
                                let transactionsDic = resultDic["data"]
                                let transactionsData = try JSONSerialization.data(withJSONObject: transactionsDic!, options: .prettyPrinted)
                                let transactions = try JSONDecoder().decode([Transation].self, from: transactionsData)
                                completion(transactions)
                            } catch {
                                print("error: \(RequestError.invalidJSONCasting)")
                                completion(nil)
                            }
            }
        } catch {
            print("Error: \(error)")
            completion(nil)
        }
        completion(nil)
    }

    func bankImage(for stringURL: String, completion: @escaping (UIImage?) -> ()) {
        if let iconUrl = URL(string: stringURL) {
            DispatchQueue.global().async {
                if let imageData = try? Data(contentsOf: iconUrl) {
                    //The image will be cached during 5 min
                    let cachableImage = CachableImage.init(identifier: String(stringURL.split(separator: "/").last!.split(separator: ".").first!),
                                       imageData: imageData,
                                       expireInterval: 60*5,
                                       creationInterval: Date.timeIntervalSinceReferenceDate)
                    CachableImageManager.saveToDisk(cachableImage: cachableImage)
                    DispatchQueue.main.async {
                        completion(UIImage(data: imageData))
                    }
                }
            }
        }
        completion(nil)
    }
    
}
