//
//  NetworkClient.swift
//  Bud
//
//  Created by Ian Andoni Magarzo Fernández on 5/10/18.
//  Copyright © 2018 Ian Magarzo Fernández. All rights reserved.
//

import Foundation

let kBaseURLBud = "http://www.mocky.io/v2/"

struct Response {
    var data: Data?
    var urlResponse: URLResponse?
    var error: Error?
}

protocol Connectable: class {
    func perform(request: URLRequest, response: @escaping (Response) -> ())
    func destroyRequests()
}

class NetworkMockClient: RequestableClient, Connectable {
    
    static var shared: RequestableClient! = NetworkMockClient()
    
    var cache: URLCache {
        return URLCache.shared
    }
    
    func perform(request: URLRequest, response: @escaping (Response) -> ()) {
        URLSession.shared.dataTask(with: request) { (data, urlResponse, error) in
            print("urlResponse: \n\(urlResponse.debugDescription)")
            if let jsonData = data {
                do {
                    let jsonObject = try JSONSerialization.jsonObject(with: jsonData)
                    let jsonPrettyData = try JSONSerialization.data(withJSONObject: jsonObject, options: JSONSerialization.WritingOptions.prettyPrinted)
                    print("data: \n\((String(data: jsonPrettyData, encoding: String.Encoding.utf8) ?? "nil"))")
                } catch {
                    print(error)
                }
            }
            let responseStruct = Response(data: data, urlResponse: urlResponse, error: error)
            response(responseStruct)
            }.resume()
    }
    
    func destroyRequests() {
        URLSession.shared.finishTasksAndInvalidate()
    }
    
}
