//
//  ImageCache.swift
//  Bud
//
//  Created by Ian Andoni Magarzo Fernández on 5/10/18.
//  Copyright © 2018 Ian Magarzo Fernández. All rights reserved.
//

import Foundation

class CachableImage: NSObject, NSCoding {
    
    var identifier: String!
    var imageData: Data!
    var expireInterval: TimeInterval!
    var creationInterval: TimeInterval!
    
    init(identifier: String, imageData: Data, expireInterval: TimeInterval, creationInterval: TimeInterval) {
        self.identifier = identifier
        self.imageData = imageData
        self.expireInterval = expireInterval
        self.creationInterval = creationInterval
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        let identifier = aDecoder.decodeObject(forKey: "identifier") as! String
        let imageData = aDecoder.decodeObject(forKey: "imageData") as! Data
        let expireInterval = aDecoder.decodeObject(forKey: "expireInterval") as! TimeInterval
        let creationInterval = aDecoder.decodeObject(forKey: "creationInterval") as! TimeInterval
        self.init(identifier: identifier, imageData: imageData, expireInterval: expireInterval, creationInterval: creationInterval)
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(identifier, forKey: "identifier")
        aCoder.encode(imageData, forKey: "imageData")
        aCoder.encode(expireInterval, forKey: "expireInterval")
        aCoder.encode(creationInterval, forKey: "creationInterval")
    }
    
}

class CachableImageManager {
    
    private class func dataBaseURL(filename: String) -> URL? {
        let url = URL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(filename)
        return url
    }
    
    public class func saveToDisk(cachableImage: CachableImage) {
        if let savedCachedImageForId = loadFromDisk(imageIdentifier: cachableImage.identifier) {
            guard (Date.timeIntervalSinceReferenceDate - savedCachedImageForId.creationInterval) > savedCachedImageForId.expireInterval else {
                print("no expiredCache")
                return
            }
            
        }
        if let url = CachableImageManager.dataBaseURL(filename: cachableImage.identifier) {
            let data = NSKeyedArchiver.archivedData(withRootObject: cachableImage)
            do {
                try data.write(to: url)
                print("image saved to disk")
            } catch {
                print("Couldn't write file")
            }
        } else {
            print("Couldn't find path")
        }
    }
    
    public class func loadFromDisk(imageIdentifier: String) -> CachableImage? {
        if let url = CachableImageManager.dataBaseURL(filename: imageIdentifier) {
            do {
                let data = try Data(contentsOf: url)
                guard let cachableImage = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(data) as? CachableImage else {
                    return nil
                }
               return cachableImage
            } catch {
                print("Error \(error)")
                return nil
            }
        } else {
            print("Error leyendo datos")
            return nil
        }
    }
}

