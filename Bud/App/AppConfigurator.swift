//
//  AppConfigurator.swift
//  Bud
//
//  Created by Ian Andoni Magarzo Fernández on 4/10/18.
//  Copyright © 2018 Ian Magarzo Fernández. All rights reserved.
//

import Foundation

class AppConfigurator {
    
    static let shared = AppConfigurator()
    
    init() {
        setupDataProvider()
    }
    
    func setupDataProvider() {
        _ = DataProvider.init(dataChannelType: .NetworkMock)
    }
    
}
