//
//  TransactionsTableViewCell.swift
//  Bud
//
//  Created by Ian Andoni Magarzo Fernández on 5/10/18.
//  Copyright © 2018 Ian Magarzo Fernández. All rights reserved.
//

import UIKit

class TransactionTableViewCell: UITableViewCell {
    
    @IBOutlet weak var bankImage: UIImageView!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var costLabel: UILabel!

}
