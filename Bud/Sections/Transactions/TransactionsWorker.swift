//
//  TransactionsWorker.swift
//  Bud
//
//  Created by Ian Andoni Magarzo Fernández on 5/10/18.
//  Copyright © 2018 Ian Magarzo Fernández. All rights reserved.
//

import Foundation
import UIKit

protocol TransactionsWorkerDelegate: class {
    func dataUpdated()
}

class TransactionsWorker {
    
    weak var delegate: TransactionsWorkerDelegate?
    var transactions: [Transation]?
    
    init() {}
    
    public func loadData() {
        DataProvider.shared.getTransctions { (transactions) in
            self.transactions = transactions
            self.delegate?.dataUpdated()
        }
    }
    
    func bankImage(for stringURL: String, completion: @escaping (UIImage?) -> ()) {
        DataProvider.shared.bankImage(for: stringURL, completion: completion)
    }
    
}
