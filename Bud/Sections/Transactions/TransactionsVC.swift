//
//  TransactionsVC.swift
//  Bud
//
//  Created by Ian Andoni Magarzo Fernández on 2/10/18.
//  Copyright © 2018 Ian Magarzo Fernández. All rights reserved.
//

import UIKit

class TransactionsViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 1))
        }
    }
    
    lazy var transactionsWorker = {
        return TransactionsWorker()
    }()
    
    lazy var tableviewDataSource = {
        return TransactionsTableViewDataSource(worker: transactionsWorker)
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = tableviewDataSource
        tableView.delegate = self
        transactionsWorker.delegate = self
        
        setupViews()
        
        activityIndicatorView.startAnimating()
        self.transactionsWorker.loadData()
    }
    
    weak var activityIndicatorView: UIActivityIndicatorView!
    private func setupViews() {
        let activityIndicatorView = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.white)
        tableView.backgroundView = activityIndicatorView
        self.activityIndicatorView = activityIndicatorView
    }
    
}

extension TransactionsViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 78.0
    }
    
}

extension TransactionsViewController: TransactionsWorkerDelegate {
    
    func dataUpdated() {
        DispatchQueue.main.async {
            self.tableView.reloadData()
            self.activityIndicatorView.stopAnimating()
        }
    }
    
}
