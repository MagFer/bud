//
//  Currencies.swift
//  Bud
//
//  Created by Ian Andoni Magarzo Fernández on 5/10/18.
//  Copyright © 2018 Ian Magarzo Fernández. All rights reserved.
//

import Foundation

enum CurrencyType: String {
    case GBP
    
    static func symbol(forCurrencyISO currency: CurrencyType) -> String {
        switch currency {
        case .GBP:
            return "£"
        }
    }
}
