//
//  DateFormatter.swift
//  Bud
//
//  Created by Ian Andoni Magarzo Fernández on 3/10/18.
//  Copyright © 2018 Ian Magarzo Fernández. All rights reserved.
//

import Foundation

extension DateFormatter {
    
    static let budDateFormat : DateFormatter = {
        let dateFormater = DateFormatter()
        dateFormater.dateFormat = "yyyy-MM-dd"
        return dateFormater
    }()
    
}
