//
//  Transaction.swift
//  Bud
//
//  Created by Ian Andoni Magarzo Fernández on 3/10/18.
//  Copyright © 2018 Ian Magarzo Fernández. All rights reserved.
//

import Foundation

struct Transation {
    let id: String?
    let date: Date?
    let description: String?
    let currency: Currency?
    let product: Product?
}

extension Transation: Codable {
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case date = "date"
        case currency = "amount"
        case description = "description"
        case product = "product"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decode(String.self, forKey: .id)
        description = try values.decode(String.self, forKey: .description)
        let dateString = try values.decode(String.self, forKey: .date)
        date = DateFormatter.budDateFormat.date(from: dateString)
        currency = try values.decode(Currency.self, forKey: .currency)
        product = try values.decode(Product.self, forKey: .product)
    }
    
}

struct Currency {
    let value: Float?
    let currencyIso: String?
}

extension Currency: Codable {
    
    enum CodingKeys: String, CodingKey {
        case value = "value"
        case currencyIso = "currency_iso"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        value = try values.decode(Float.self, forKey: .value)
        currencyIso = try values.decode(String.self, forKey: .currencyIso)
    }
    
}

struct Product: Codable {
    let id: Int?
    let title: String?
    let icon: String?
}
