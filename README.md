## Mockup

Design mockup with a **transaction list** screen in [Sketch Cloud](https://sketch.cloud/s/Poa5L)

---

## API

Api documentation [Postman](https://documenter.getpostman.com/view/72247/RWgnWzrb)
