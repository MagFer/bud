//
//  BudTests.swift
//  BudTests
//
//  Created by Ian Andoni Magarzo Fernández on 2/10/18.
//  Copyright © 2018 Ian Magarzo Fernández. All rights reserved.
//

import XCTest
@testable import Bud

class BudTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testMockRequests() {
        let transactionsExpectation = expectation(description: "Transaction Serialization")
        
        _ = DataProvider.init(dataChannelType: .LocalMock)
        DataProvider.shared.getTransctions { (transactions) in
            print("Transactions: \(transactions.debugDescription)")
            XCTAssertNotNil(transactions)
            transactionsExpectation.fulfill()
        }
        
        waitForExpectations(timeout: 1, handler: nil)
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
